###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (https://middlemanapp.com/advanced/dynamic_pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
# configure :development do
#   activate :livereload
# end

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

activate :blog do |blog|
  blog.layout = "post"

end

activate :livereload

activate :directory_indexes

set :partials_dir, "partials"

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

class MyAwesomeMapper < ContentfulMiddleman::Mapper::Base
  def map(context, entry)
    super
    # After calling super the context object
    # will have a property for every field in the
    # entry
  end
end

activate :contentful do |f|
  f.space = { name: "mgwp8un6azr6" }
  f.access_token = "35675f7eafe06e0e0352bacdd73b8f1ce67af4280e46a285adf66573c3e84fdd"
  f.cda_query = {}

  # f.cda_query = {}
  # f.dynamic_entries = :auto
  f.content_types = {
    mission: "5nB8qCxL8W4uyUiKeAoG0a", 
    board: "21ehFFINP2uGYyioC0k2i0", 
    banner: "7gV3xIJrZSeqookUsy8KEa", 
    notification: "1yptvzdcR2yMkUMieAsqqU", 
    press: "4LqwckQRzGE24soEiwCw8S"
  }
end
# activate :contentful do |f|
#   f.space = { name: "mgwp8un6azr6" }
#   f.access_token = "35675f7eafe06e0e0352bacdd73b8f1ce67af4280e46a285adf66573c3e84fdd"
#   # f.cda_query = {}
#   # f.dynamic_entries = :auto
#   f.content_types = { board:  }
# end








# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end
